var Question = require('../model/Question');

exports.answerAmongPossible = function (question, country) {
    return question.answers.includes(country);
}

exports.getQuestionById = function (qId, callback) {
    Question.find({qId: qId}).lean().limit(1).exec(function (err, result) {
        if (err || result.length == 0) {
            callback();
        } else {
            callback(result[0]);
        }
    });
};

exports.getRandomQuestion = function (callback) {
    Question.countDocuments(function (err, count) {
        if (err) {
            return console.log(err);
        }
        var rand = Math.floor(Math.random() * count);
        Question.findOne().lean().skip(rand).exec(function (err, result) {
            if (err) {
                callback()
            } else {
                console.log(result);
                callback(result);
            }
        });
    });
};
