exports.transformToDTO = function (question) {
    var arr = question.answers;
    arr.sort(() => Math.random() - 0.5)
    return {
        qId: question.qId,
        question: question.question,
        possibleAnswers: arr
    };
}
