var mongo = require('mongoose');
var mongoDB = 'mongodb://admin:admin@172.30.84.115:27017/sampledb';
var Question = require('../model/Question');
var Country = require('../model/Country');
const fs = require('fs');


exports.connect = function () {
    mongo.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true}).then(exports.prepareData);
    var db = mongo.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));
};

exports.prepareData = function () {
    Country.deleteMany({}, exports.insertCountries);

};
exports.insertCountries = function () {
    fs.readFile('countries.json', (err, data) => {
        if (err) throw err;
        let countries = JSON.parse(data);
        countries.forEach(country => {
            var c = new Country({
                countryName: country.name
            });
            c.save(function (err) {
                if (err) console.log(err);
                else {
                    console.log("Successfully saved country with name " + country.name);
                }
            })
        });
        exports.prepareQuestions(countries);

    });
};
exports.prepareQuestions = function (countries) {
    Question.deleteMany({}, () => {
        exports.insertQuestions(countries)
    });
};
exports.insertQuestions = function (countries) {
    fs.readFile('questions.json', (err, data) => {
        if (err) throw err;
        let questions = JSON.parse(data);
        var counter = 0;
        questions.forEach(question => {
            var ans = exports.getRandomCountries(countries, question.rightAnswer);
            var q = new Question({
                qId: ++counter,
                question: question.question,
                rightAnswer: question.rightAnswer,
                answers: ans
            });
            console.log("Id"+counter);
            console.log("Answers" + ans);
            q.save(function (err) {
                if (err) console.log(err);
                else {
                    console.log("Successfully saved question with id " + q.qId);
                }
            })
        });

    });
};
exports.getRandomCountries = function (countries, except) {
    var resultArray = [except];
    count = 0;
    while (count < 3) {
        var cRandom = countries[Math.floor(Math.random() * countries.length)].name;
        if (!resultArray.includes(cRandom)) {
            resultArray.push(cRandom);
            count++;
        }
    }
    return resultArray;
};
