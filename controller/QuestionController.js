var connector = require('../utils/connector');
var service = require('../service/questionService');
var transformer = require('../service/questionTransformer');

exports.randomQuestion = function(req, res) {
    service.getRandomQuestion(function (question) {
        if (!question) {
            connector.sendCode(500, res)
        } else {
            connector.sendData(200, transformer.transformToDTO(question), res);
        }
    });

};
exports.rightAnswer = function(req, res) {
    var answer = req.body;
    if (!answer) {
        connector.sendCodeWithMessage(400, 'No answer provided', res);
        return;
    }
    if (!answer.qId || answer.qId === '') {
        connector.sendCodeWithMessage(400, 'Question Id cannot be empty', res);
        return;
    }
    if (!answer.country || answer.country === '') {
        connector.sendCodeWithMessage(400, 'Country cannot be empty', res);
        return;
    }
    service.getQuestionById(answer.qId, function (question) {
        if (!question) {
            connector.sendCodeWithMessage(404, 'Question does not exists', res);
            return;
        }
        if (!service.answerAmongPossible(question, answer.country)) {
            connector.sendCodeWithMessage(400, "Answer must be among possible answers", res);
            return;
        }
        if (answer.country === question.rightAnswer) {
            connector.sendCodeWithMessage(202, "Well done!", res);
        } else {
            connector.sendCodeWithMessage(406, "Try again...", res);
        }
    });
};

exports.getAnswer = function (req, res) {
    const qId = req.query.qId;
    if (!qId || qId === '') {
        connector.sendCodeWithMessage(400, 'Question Id cannot be empty', res);
        return;
    }
    service.getQuestionById(qId, function (question) {
        if (!question) {
            connector.sendCodeWithMessage(404, 'Question does not exists', res);
        } else {
            connector.sendData(200, {"question": question.question, "answer": question.rightAnswer}, res);
        }
    });

};

