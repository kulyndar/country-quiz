var rest_countries = require('../utils/restCountriesEndpoint');
var map = require('../utils/mapEndpoint');
var connector = require('../utils/connector');

exports.getCountryInfo = function (req, res) {
    if (!req.query.name || req.query.name == '') {
        connector.sendCodeWithMessage(400, 'Country cannot be empty', res);
        return;
    }
    rest_countries.findByName(req.query.name, function(error, response, body){
        if (error) {
            connector.sendCode(404, res);
        } else if (response.statusCode == 404 ) {
            connector.sendCode(404, res);
        } else {
            var obj = JSON.parse(body);
            map.geocode(req.query.name,
                function () {
                    connector.sendCode(404, res);
                },
                function (mapObj) {
                    connector.sendData(200, {country: obj[0], map: mapObj}, res);
                })

        }
    });

}