var mapycz = require('mapy-cz');

exports.geocode = function (value, errorCallback, successCallback) {
    var coord = new mapycz(value);
    coord.on("error", errorCallback);
    coord.on("end", function (response) {
        var item = response.result.point["0"].item["0"].$;
        successCallback(item);
    })
}