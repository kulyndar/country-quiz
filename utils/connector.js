exports.sendCode = function (code, res) {
    exports.sendCodeWithMessage(code, "", res);
};
exports.sendCodeWithMessage = function (code, message, res) {
    res.status(code).json({"code": code, "message": message});
};
exports.sendData = function (code, data, res) {
    res.status(code).json(data);
};
