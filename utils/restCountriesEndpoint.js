var request = require('request');

const restCountries = 'https://restcountries.eu/rest/v2/';
const path_name = 'name/';

exports.findByName = function(name, callback) {
request(
    restCountries+path_name+name,
    function(error, response, body) {
        callback(error, response, body);
    }
);
};
