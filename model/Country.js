var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var CountrySchema = new Schema({
    countryName : String
});

var Country = mongoose.model('Country', CountrySchema);
module.exports = Country;