var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var QuestionSchema = new Schema({
    qId: Number,
    question : String,
    answers: [{type: String}],
    rightAnswer: String
});

var Question = mongoose.model('Question', QuestionSchema);
module.exports = Question;