# Country QUIZ

Country quiz je jednoduchá hra. Pokud jste zvědavý člověk a rád dozvíte něco nového, určitě vás pobaví.

Průběh hry je následující: uživatel dostane 10 otázek o různých zemích. Za kažsou správnou odpověď dostává 10 bodů. Po každé odpovědí uživatel má možnost zobrazit zajímavé informace o státu. 

## Použité technologie

Country QUIZ je client-server aplikace. Jeji kontrou je server, který poskytuje REST API. To znamená, že libovolný uživatel může vytvořit vlastní frontend a používat aplikaci podle svých potřeb.

Server je napsán pomocí technologie NodeJS. Kromě vlasního API poskytuje data z veřejných API RESTCountries a Mapy.cz.

V aplikaci je použitá databáze MongoDB, kde se ukládají otázky a země.

Client je jednoduchá React apliakce, která komunikuje se serverrem přes HTTP.

## Odkazy
Server:
- https://cutt.ly/countryquiz
- http://country-quiz-git-country-quiz.apps.us-west-1.starter.openshift-online.com/game

Client:
- https://cutt.ly/via-game
- http://client-country-quiz-country-quiz.apps.us-west-1.starter.openshift-online.com/