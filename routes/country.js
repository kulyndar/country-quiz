
var controller = require('../controller/APIController');
var express = require('express');
var router = express.Router();

/**
 * @typedef Country
 * Complete object from RESTCountries API.
 * @property{string} name - example:  Colombia,
 * @property{string[]} topLevelDomain - example:  [.co],
 * @property{string} alpha2Code - example:  CO,
 * @property{string} alpha3Code - example:  COL,
 * @property{string[]} callingCodes - example:  [57],
 * @property{string} capital - example:  Bogotá,
 * @property{string[]} altSpellings - example:  [CO, Republic of Colombia, República de Colombia],
 * @property{string} region - example:  Americas,
 * @property{string} subregion - example:  South America,
 * @property{string} population - example:  48759958,
 * @property{string[]} latlng - example:  [4.0, -72.0],
 * @property{string} demonym - example:  Colombian,
 * @property{string} area - example:  1141748.0,
 * @property{string} gini - example:  55.9,
 * @property{string[]} timezones - example:  [UTC-05 - example: 00],
 * @property{string[]} borders - example:  [BRA, ECU, PAN, PER, VEN],
 * @property{string} nativeName - example:  Colombia,
 * @property{string} numericCode - example:  170,
 * @property{string[]} currencies - example:  [{ code - example:  COP, name - example:  Colombian peso, symbol - example:  $ } ],
 * @property{string[]} languages - example:  [{iso639_1 - example:  es, iso639_2 - example:  spa, name - example:  Spanish, nativeName - example:  Español} ],
 * @property{string[]} translations - example:  {de - example:  Kolumbien, es - example:  Colombia} ,
 * @property{string} flag - example:  https://restcountries.eu/data/col.svg,
 * @property{string[]} regionalBlocs - example:  [{acronym - example:  PA, name - example:  Pacific Alliance, otherAcronyms - example:  [], otherNames - example:  [Alianza del Pacífico]} , {acronym - example:  USAN}]
 */
/**
 * @typedef Map
 * @property {integer} x - x coord
 * @property {integer} y - y coord
 */
/**
 * @typedef CountryWithMap
 * @property {Country.model} country - country information from RESTCountries API
 * @property {Map.model} map - map coords from Mapy.cz geocoding API
 */
/**
 * Returns information about country. This is an endpoind for 2 APIs - RESTCountries (https://restcountries.eu/) and Mapy.cz (https://api.mapy.cz/)
 * @route GET /country
 * @group Country information
 * @param {string} name.query.required - name of the country
 * @produces application/json
 * @returns {CountryWithMap.model} 200 - country information from RESTCountries API and map coords from Mapy.cz geocoding API
 * @returns {Error.model} 404 - country wasn`t found either in RESTCountries or in Mapy.cz API
 * @returns {Error.model}  500 - unexpected error
 */
router.get('/', controller.getCountryInfo);

module.exports = router;
