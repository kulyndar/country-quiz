var express = require('express');
var router = express.Router();
var controller = require('../controller/QuestionController');

/**
 * @typedef Question
 * @property {integer} qId - question ID
 * @property {string} question - the text of the question
 * @property {[string]} possibleAnswers - list of possible answers
 */

/**
 * @typedef Answer
 * @property {integer} qId.required - question ID
 * @property {string} country.required - answer.
 */

/**
 * @typedef QuestionWithAnswer
 * @property {string} question - the text of the question
 * @property {string} answer - correct answer
 */
/**
 * @typedef Error
 * @property {integer} code - HTTP status code
 * @property {string} message - message text
 */

/**
 * Gets random question with list of possible answers.
 * @route GET /game
 * @group Game
 * @produces application/json
 * @returns {Question.model} 200 - random question with possible answers
 * @returns {Error.model}  500 - unexpected error
 */
router.get('/', controller.randomQuestion);

/**
 * Detects if the answer is correct or not.
 * @route POST /game
 * @group Game
 * @param {Answer.model} answer.body.required - user answer. Country in the answer must be among of the possibleAnswers
 * @consumes application/json
 * @produces application/json
 * @returns {object} 200 - correct answer
 * @returns {object} 406 - incorrect answer
 * @returns {Error.model} 404 - question does not exists
 * @returns {Error.model} 400 - incorrect request
 */
router.post('/', controller.rightAnswer);

/**
 * Hets the right answer for the question with defined ID
 * @route GET /game/answer
 * @group Game
 * @param {integer} qId.query.required - question ID
 * @produces application/json
 * @returns {QuestionWithAnswer.model} 200 - answer for the question
 * @returns {Error.model} 404 - question does not exists
 * @returns {Error.model} 400 - incorrect request
 */
router.get('/answer', controller.getAnswer)

module.exports = router;
