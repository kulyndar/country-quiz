var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var lessMiddleware = require('less-middleware');
var logger = require('morgan');
var dbService = require('./service/dbService');
var cors = require('cors')


var indexRouter = require('./routes/game');
var countryRouter = require('./routes/country');

var app = express();
app.use(cors());
const expressSwagger = require('express-swagger-generator')(app);
let options = {
  swaggerDefinition: {
    info: {
      description: 'Application CountryQuizzz is a simple game. It uses 2 external APIs.' +
          'Application doesn`t store any points or sessions. It just gets the question and the answer.' +
          'So the task to store session info is on the user (primary store question ID)',
      title: 'Country Quizzz',
      version: '1.0.0',
    },
    host: 'localhost:3001',
    basePath: '/',
    produces: [
      "application/json"
    ],
    schemes: ['http'],
  },
  basedir: __dirname, //app absolute path
  files: ['./routes/*.js'] //Path to the API handle folder
};
expressSwagger(options)
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(lessMiddleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/game', indexRouter);
app.use('/country', countryRouter);


// catch 404 and forward to error handler
app.use('/404', function(req, res, next) {
  next(createError(404));
});
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
dbService.connect();

/*LOCALS*/
app.locals.print_number = function(obj){
  return obj.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
};

module.exports = app;
