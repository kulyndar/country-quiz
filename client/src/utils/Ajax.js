
export const BACKEND_URI = "http://country-quiz-git-country-quiz.apps.us-west-1.starter.openshift-online.com";

var Ajax = {
    get: function(url, onSuccess, onError){
        fetch(BACKEND_URI+url).then(response=>{
            if (onSuccess){
                onSuccess(response);
            }
        }, error => {
            if (onError){
                onError(error);
            }
        })
    },
    post: function(url, body, onSuccess, onError){
        const options = {
            method: 'POST',
            body: body,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }
        console.log("POST request")
        fetch(BACKEND_URI+url, options).then(response=>{
            console.log("Here is the answer")
            if (onSuccess){
                onSuccess(response);
            }
        }, error=>{
            if (onError){
                onError(error);
            }
        })
    }
};
export default Ajax;

