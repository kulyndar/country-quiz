import React from 'react';
import './App.css';
import Game from './inc/Game';
import {Button, Carousel} from "react-bootstrap";
import {BACKEND_URI} from "./utils/Ajax";

var createReactClass = require("create-react-class");
var App = createReactClass({
  getInitialState(){
    return {
      play:false
    }
  },
  render: function() {
    if (this.state.play) {
      return (
            <Game playEndHandler={this.again}/>
      );
    } else {
      return (
          <div>
              <div className="carouselTitle">
                  <h1 >Country QUIZZZ</h1>
                  <p className="invitation">Welcome to the Country Quiz. This is the app with interesting questions about different countries. Can you answer all of them?</p>
                  <div className="buttons">
                      <Button variant="primary" size="lg" href={BACKEND_URI+"/api-docs"} target="_blank">Docs</Button>
                      <Button variant="success" size="lg" onClick={()=>{this.setState({play:true})}} >Play</Button>
                  </div>
              </div>

            <Carousel interval="2000">
              <Carousel.Item>
                <img
                    className="d-block  fit"
                    src={process.env.PUBLIC_URL+"/first.jpg"}
                    alt="First slide"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                    className="d-block  fit"
                    src={process.env.PUBLIC_URL+"/third.jpg"}
                    alt="Third slide"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                    className="d-block  fit"
                    src={process.env.PUBLIC_URL+"/forth.jpg"}
                    alt="Third slide"
                />
              </Carousel.Item>
            </Carousel>
          </div>
      )
    }
  },
  again: function(){
    this.setState({
      play:false
    })
  }
})

export default App;
