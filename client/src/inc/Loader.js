import React from 'react';
import Spinner from 'react-bootstrap/Spinner';

var createReactClass = require('create-react-class');

var Loader = createReactClass({
    getInitialState: function(){
        return {
            show: true
        }
    },
    render : function(){
        if (this.state.show){
            return <Spinner className="spinner" animation="border" variant="primary" />;
        } else {
            return <div></div>;
        }

    },
    componentDidMount: function(){
        if (this.state.show && this.props.timeout){
            var L = this;
            setTimeout(function(){
                L.setState({
                    show: false
                })
            }, this.props.timeout)
        }
    }
});

export default Loader;