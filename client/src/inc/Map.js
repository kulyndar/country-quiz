import React from 'react';
import Loader from "./Loader";
var createReactClass = require('create-react-class');

var Map = createReactClass({
    getInitialState: function() {
        return {
            posX: this.props.x,
            posY: this.props.y
        };
    },
    componentWillMount: function(){
        window.Loader.lang = "en";
        window.Loader.load();
    },
     render: function(){
         return (<div className="mapHolder">
             <Loader timeout="2000" />
             <div id="m"></div>
         </div>);
     },
    componentDidMount: function() {
            var POS = {x: this.state.posX, y: this.state.posY};
            setTimeout(function () {
                var coords = window.SMap.Coords.fromWGS84(POS.x, POS.y);
                var m = new window.SMap(window.JAK.gel("m"), coords, 2);
                m.addControl(new window.SMap.Control.Sync());
                m.addDefaultLayer(window.SMap.DEF_BASE).enable();
                m.addDefaultControls();
                var layer = new window.SMap.Layer.Marker();
                m.addLayer(layer);
                layer.enable();
                var marker = new window.SMap.Marker(coords);
                layer.addMarker(marker);
            }, 2000)

    }
});

export default Map;