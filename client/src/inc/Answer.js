import React from 'react';
import Loader from "./Loader";
import CountryInfo from "./CountryInfo";
import Ajax from "../utils/Ajax";
import ErrorHandler from "./ErrorHandler";
import {Container, Row, Col, Button, Image} from "react-bootstrap";

var createReactClass = require('create-react-class');
const QUESTION_URI = "/game";

var Answer = createReactClass({
    getInitialState: function(){
      return  {
        ready: false,
        rightAnswer: null,
          isRight: false,
          error: null
      };
    },
    render: function(){
    if (!this.state.ready){
        console.log("Loader aaa");
        return <Loader />;
    } else {
        if (this.state.error) {
            return <ErrorHandler errorCode={this.state.error} />
        } else {
            var modal = <CountryInfo ref="myModal" countryName={this.state.rightAnswer} />
            var answerText;
            var answerImage;
            var additionText;
            if (this.state.isRight) {
                answerText=(
                    <Row><h1>Well done!</h1></Row>
                );
                answerImage=(
                    <Row>
                    <Image className="answerImg" src={process.env.PUBLIC_URL+"/correct.png"}  />
                    </Row>
                );
                additionText=(
                    <Row className="additional"><p>You know geography like the back of your hand!</p></Row>
                );
            } else {
                answerText=(
                    <Row><h1>Maybe next time...</h1></Row>
                );
                answerImage=(
                    <Row>
                    <Image className="answerImg" src={process.env.PUBLIC_URL+"/wrong.jpg"}  />
                    </Row>
                );
                additionText=(
                    <Row className="additional"><p>Right answer was {this.state.rightAnswer}, your answer was {this.props.answer}</p></Row>
                );
            }
            var buttons= (
                <div className="buttons">
                    <Button variant="outline-primary" onClick={()=>{this.refs.myModal.handleOpen()}}>I want to know more!</Button>
                    <Button variant="outline-success" onClick={()=>{this.props.nextCallback(this.state.isRight)}}>Continue</Button>
                </div>
            );
            return (
                <Container>
                    {answerImage}
                    {answerText}
                    {additionText}
                    {buttons}
                    {modal}
                </Container>
            )
        }
    }

    },
    componentDidMount: function(){
        if (!this.state.ready){
            var body = this.createAnswer();
            console.log(body)
            Ajax.post(QUESTION_URI, body, this.handleSuccess, this.handleError);
        }
    },
    createAnswer:function(){
        console.log("HERE I AM. COUNTRY IS "+this.props.answer)
        return  JSON.stringify({
            qId: this.props.qId,
            country: this.props.answer
        });
    },
    handleSuccess: function(response){
        if (response.ok){
            console.log("this is correct");
            this.setState({
               ready: true,
               isRight: true,
               rightAnswer: this.props.answer
            });
        } else {
            console.log("not correct");
            this.handleError(response);
        }

    },
    handleError: function (response) {
        if (response.status === 406){
            console.log("get right answer");
            Ajax.get(QUESTION_URI+"/answer?qId="+this.props.qId, this.handleRightAnswerSuccess, this.handleError);
        } else {
            console.log("error!");
            console.log(response);
            this.setState({
                ready: true,
                error: response.status
            });
        }
    },
    handleRightAnswerSuccess: function(response){
        if (response.ok) {
            response.json().then(this.parseRightAnswer);
        } else {
            this.handleError(response);
        }
    },
    parseRightAnswer: function(body){
        console.log("parseRightAnswer");
        this.setState({
            ready: true,
            isRight: false,
            rightAnswer: body.answer
        })
    }
});
export default Answer;