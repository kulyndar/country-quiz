import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Loader from './Loader';
import Button from 'react-bootstrap/Button';
import Ajax from '../utils/Ajax';
import ErrorHandler from "./ErrorHandler";
import Map from './Map';
import {Container, Row, Col} from 'react-bootstrap';
import Media from 'react-bootstrap/Media';

var createReactClass = require('create-react-class');
const COUNTRY_URL = "/country?name=";

var CountryInfo = createReactClass({
    getInitialState: function () {
        return {
            ready: false,
            country: null,
            map: null,
            show: false,
            errorType: null
        };
    },
    render: function () {
        let content;
        if (!this.state.ready) {
            content = <Loader/>;
        } else {
            if (this.state.errorType) {
                content = <ErrorHandler errorCode={this.state.errorType} what="Country"/>
            } else {
                content = (
                    <Container>
                        <Row>
                            <Media>
                                <img width={100} src={this.state.country.flag} className="mr-3" />
                                <Media.Body>
                                    <p><b>Native name: </b> {this.state.country.nativeName}</p>
                                    <p><b>Region: </b>{this.state.country.region} ({this.state.country.subregion})</p>
                                    <p><b>Population: </b>{this.state.country.population}</p>
                                    <p><b>Area: </b>{this.state.country.area} m<sup>2</sup></p>
                                    <p><b>Languages: </b>{this.state.country.languages.map(lang => lang.name + ' (' + lang.nativeName + ')').join(', ')}</p>
                                </Media.Body>
                            </Media>
                        </Row>
                        <Row>
                            <Map x={this.state.map.x} y={this.state.map.y}/>
                        </Row>
                    </Container>
                )
            }
        }
        return (
            <Modal show={this.state.show} size="lg" onHide={this.handleClose} centered>
                <Modal.Header closeButton>
                    <Modal.Title>{this.props.countryName}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {content}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={this.handleClose}>Close</Button>
                </Modal.Footer>
            </Modal>
        );
    },
    handleClose: function () {
        this.setState({show: false});
    },
    handleOpen: function () {
        this.setState({show: true});
    },
    componentDidMount: function () {
        if (!this.state.ready) {
            Ajax.get(COUNTRY_URL + this.props.countryName, this.successHandler, this.errorHandler);
        }
    },
    successHandler: function (response) {
        if (response.ok) {
            var CI = this;
            response.json().then(function(json){
                CI.setState({
                    country: json.country,
                    map: json.map,
                    ready: true,
                });
            });

        } else {
            if (response.status == 404) {
                this.notFoundHandler(response);
            } else {
                this.errorHandler(response);
            }
        }
    },
    errorHandler: function (error) {
        this.setState({
            errorType: 500,
            ready: true
        })
    },
    notFoundHandler: function (response) {
        this.setState({
            errorType: 404,
            ready: true
        })
    }

});

export default CountryInfo;