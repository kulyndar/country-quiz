import React from 'react';
import {Container, Row, Col, ProgressBar, Button, Carousel} from 'react-bootstrap';
import Question from './Question';
import Answer from './Answer';
import {BACKEND_URI} from "../utils/Ajax";
var createReactClass = require('create-react-class');

var Game = createReactClass({
    getInitialState: function(){
        return {
            qNum: 1,
            points: 0,
            endGame: false,
            questionTime: true,
            qId:null,
            answer:null
        }
    },

    render: function(){
        if (!this.state.endGame) {
            var game;
            if (this.state.questionTime) {
                game = <Question qNum={this.state.qNum} sendAnswer={this.answerHandler} />
            } else {
                game = <Answer qId={this.state.qId} answer={this.state.answer} nextCallback={this.handleAnswer} />
            }
            var content = (
                <Container className="w80">
                    <Row className="">
                        <div className="buttons">
                            <Button variant="outline-danger" size="lg" onClick={()=>{this.props.playEndHandler()}}>Quit</Button>
                            <Button variant="outline-primary" size="lg" href={BACKEND_URI+"/api-docs"} target="_blank">Docs</Button>
                        </div>
                    </Row>
                    <Row><h1 className="mainTitle">Country QUIZZZ</h1></Row>

                    <Row>
                        <ProgressBar now={this.state.qNum * 10} label={`${this.state.qNum}/10`}/>
                    </Row>
                    <Row>
                        <Col className="points">Points: </Col>
                        <Col>{this.state.points}</Col>
                    </Row>
                    <Row className="questionContainer">
                        {game}
                    </Row>
                </Container>
            )
            return content;
        } else {
            return (
                <div>
                    <div className="carouselTitle">
                        <Container className="end">
                            <Row><h1 className="mainTitle">Country QUIZZZ</h1></Row>
                            <Row>
                                <h2>Game over. Your point are {this.state.points}</h2>
                            </Row>
                            <Row >
                                <div className="buttons w100">
                                    <Button variant="success" size="lg" onClick={()=>{this.props.playEndHandler()}}>Main page</Button>
                                    <Button variant="primary" size="lg" href={BACKEND_URI+"/api-docs"} target="_blank">Docs</Button>
                                </div>
                            </Row>
                        </Container>
                    </div>

                    <Carousel interval="2000">
                        <Carousel.Item>
                            <img
                                className="d-block  fit"
                                src={process.env.PUBLIC_URL+"/first.jpg"}
                                alt="First slide"
                            />
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                                className="d-block  fit"
                                src={process.env.PUBLIC_URL+"/third.jpg"}
                                alt="Third slide"
                            />
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                                className="d-block  fit"
                                src={process.env.PUBLIC_URL+"/forth.jpg"}
                                alt="Third slide"
                            />
                        </Carousel.Item>
                    </Carousel>
                </div>
            )
        }
    },
    answerHandler: function(qId, answer){
        this.setState({
            qId: qId,
            answer: answer,
            questionTime: false
        });
    },
    handleAnswer: function(rightAnswer){
        var isEnd = this.state.qNum === 10;
        if (rightAnswer){
            this.setState({
                points: this.state.points + 10,
                questionTime: true,
                endGame: isEnd,
                qNum: this.state.qNum+1
            });
        } else {
            this.setState({
                points: this.state.points,
                questionTime: true,
                endGame: isEnd,
                qNum: this.state.qNum+1
            });
        }
    }

});
export default Game;