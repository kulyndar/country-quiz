import React from 'react';
import Alert from 'react-bootstrap/Alert';

var createReactClass = require('create-react-class');

var ErrorHandler = createReactClass({
    render: function(){
        let content;
        const heading = "Ooops, something went wrong!"
        if (this.props.errorCode == 404){
            const isWhatMesage=  " wasn`t found. Please check the correctness of your data";
            const noWhatMessage = "Server responded with status 404";
            var message = this.props.what ? this.props.what + isWhatMesage : noWhatMessage;
            
        } else if (this.props.errorCode == 500) {
            const message = "Server responded with status 500";
        } else {
            const message = "Unknown error";
        }
        return (
            <Alert variant="danger">
                <Alert.Heading>{heading}</Alert.Heading>
                {message}
            </Alert>
        )
    }
});
export default ErrorHandler;