import React from 'react';
import Loader from './Loader';
import Ajax from "../utils/Ajax";
import ErrorHandler from "./ErrorHandler";
import {Container, Row, Col} from 'react-bootstrap';
import Button from 'react-bootstrap/Button';

var createReactClass = require('create-react-class');
const QUESTION_URI = "/game";

var Question = createReactClass({
    getInitialState: function(){
        return {
          ready: false,
            qId: null,
            question:null,
            answers:[],
            error:null
        };
    },
    render: function(){
        if (!this.state.ready){
            return <Loader/>
        } else {
            if (this.state.error){
                return <ErrorHandler errorCode={this.state.error} />
            } else {
                var buttons = (
                    <Container>
                    <Row>
                        <Col>
                            <Button variant="primary" size="lg" block onClick={()=>this.sendAnswer(this.state.answers[0])}>
                                {this.state.answers[0]}
                            </Button>
                        </Col>
                        <Col>
                            <Button variant="primary" size="lg" block onClick={()=>this.sendAnswer(this.state.answers[1])}>
                                {this.state.answers[1]}
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Button variant="primary" size="lg" block onClick={()=>this.sendAnswer(this.state.answers[2])}>
                                {this.state.answers[2]}
                            </Button>
                        </Col>
                        <Col>
                            <Button variant="primary" size="lg"  block onClick={()=>this.sendAnswer(this.state.answers[3])}>
                                {this.state.answers[3]}
                            </Button>
                        </Col>
                    </Row>
                    </Container>

                );
                var content = (
                    <Container>
                        <Row>
                            <h2>Question {this.props.qNum}</h2>
                        </Row>
                        <Row>
                            <p>{this.state.question}</p>
                        </Row>
                        {buttons}
                    </Container>
                );
                return content;
            }
        }
    },
    componentDidMount: function(){
        if (!this.state.ready){
            this.getQuestion();
        }
    },
    getQuestion(){
        Ajax.get(QUESTION_URI, this.handleSuccess, this.handleError);
    },
    handleSuccess: function(response){
        console.log(response);
        var Q = this;
       if (response.ok) {
           response.json().then(function(body){
               console.log(body);
               Q.setState({
                   qId: body.qId,
                   question: body.question,
                   answers: body.possibleAnswers,
                   ready: true
               });
           });
       } else {
           this.setState({
               error: response.status,
               ready: true
           });
       }
    },
    handleError: function(error){
        this.setState({
            error: error.status,
            ready: true
        })
    },
    sendAnswer: function(answer){
        this.props.sendAnswer(this.state.qId, answer);
    }
});
export default Question;