var coords = SMap.Coords.fromWGS84(coordX, coordY);
var m = new SMap(JAK.gel("m"), coords, 2);
m.addControl(new SMap.Control.Sync());
m.addDefaultLayer(SMap.DEF_BASE).enable();
m.addDefaultControls();
var layer = new SMap.Layer.Marker();
m.addLayer(layer);
layer.enable();


var marker = new SMap.Marker(coords);
layer.addMarker(marker);

